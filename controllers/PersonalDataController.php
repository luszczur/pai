<?php

session_start();

require_once "AppController.php";

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class PersonalDataController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function personaldata()
    {
		
		$mapper = new UserMapper();
		
		$mapper->personaldata();

        header("Location: views/PersonalDataController/personaldata.php");
    }
}