<?php

//session_start();

require_once "AppController.php";

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class LoggedController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }

	
	public function logged()
    {
        $this->render('logged');
    }
	

    public function upload()
    {
		//echo "hello world";
		
		$mapper = new UserMapper();
		
        if($this->isPost()) 
		{
			$file = $_FILES['image'];
			
			$fileName = $_FILES['image']['name'];
			//$fileTmpName = $_FILES['image']['tmp_name'];
			$fileSize = $_FILES['image']['size'];
			$fileType = $_FILES['image']['type'];
			//$fileError = $_FILES['image']['error'];
			
			$fileExt = explode('.', $fileName);
			$fileActualExt = strtolower(end($fileExt));
			
			$allowed = array('jpg', 'jpeg', 'png');
			
			if(in_array($fileActualExt, $allowed))
			{
				if($fileSize < 64)
				{
					$data = file_get_contents($_FILES['image']['tmp_name']);
			
					$_SESSION['imagename'] = $fileName;
					$_SESSION['imagedata'] = $data;
					$_SESSION['imagetype'] = $fileType;
			
					$mapper->uploadImage();
					$mapper->proxyUserImage();
					
					header("Location: views/LoggedController/logged.php");
				}
				else {
				
					echo "Size of the file is too big!";
					header("Location: views/LoggedController/logged.php");
					exit();
				}
			} 
			else {
				echo "Wrong type of the file!";
				header("Location: views/LoggedController/logged.php");
			}
            
        }
        header("Location: views/LoggedController/logged.php");
    }
}
