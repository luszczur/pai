<?php

require_once "AppController.php";

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class RegisterController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }


    public function register()
    {
		//echo "hello world";
		
		$mapper = new UserMapper();
		
        if($this->isPost()) 
		{
			//echo "hello world2";
            if ($mapper->checkIfEmailExists($_POST['email'])) 
			{
                return $this->render('register', ['message' => ['User with that email already exists']]);
            } 
			else 
			{
                $mapper->setUser($_POST['name'], $_POST['surname'], $_POST['email'], md5($_POST['password']));

                //$url = "http://$_SERVER[HTTP_HOST]/";
                header("Location: /pai");
                exit();
            }
        }

        $this->render('register');
    }
}
