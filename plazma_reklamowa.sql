-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 04 Lut 2019, 23:54
-- Wersja serwera: 10.1.37-MariaDB
-- Wersja PHP: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `plazma_reklamowa`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `adres`
--

CREATE TABLE `adres` (
  `id` int(11) NOT NULL,
  `miasto` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `ulica` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `numer` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `firma`
--

CREATE TABLE `firma` (
  `id` int(11) NOT NULL,
  `nazwa_firmy` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `adres` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `image`
--

CREATE TABLE `image` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `image`
--

INSERT INTO `image` (`id`, `name`, `type`, `image`) VALUES
(32, 'DSC_0034.JPG', '', '');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `konto`
--

CREATE TABLE `konto` (
  `id` int(11) NOT NULL,
  `srodki` int(11) NOT NULL,
  `karta` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `ostatnia_transakcja` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `konto`
--

INSERT INTO `konto` (`id`, `srodki`, `karta`, `ostatnia_transakcja`) VALUES
(10, 50, 'Mastercard', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `proxy_user_image`
--

CREATE TABLE `proxy_user_image` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `transakcja`
--

CREATE TABLE `transakcja` (
  `id` int(11) NOT NULL,
  `cel` int(11) NOT NULL,
  `kwota` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `konto` int(11) NOT NULL,
  `adres` int(11) NOT NULL,
  `firma` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id`, `name`, `surname`, `email`, `password`, `konto`, `adres`, `firma`) VALUES
(6, 'Katarzyna', 'Oleksy', 'oleksy@gmail.com', '29100f83238dc292b735aed6f03bfedd', 0, 0, 0),
(7, 'Åukasz', 'Szczurek', 'lukasz.szczurek@gmail.com', '0f5575ffdb68fbe1d315bb055db67f22', 0, 0, 0),
(8, 'Piotr', 'Sroga', 'piotr.sroga@gmail.com', '233d1e3d00abd7eadc747cc0fed5c12f', 10, 0, 0),
(9, 'Kamil', 'Slowinski', 'slowinski@onet.pl', 'e6c3d01c1d20a0482445e67ac3cf541e', 0, 0, 0),
(10, 'Andrzej', 'Sapkowski', 'Sapkowski@interia.pl', '037066dfb06356184e35a9eefa80b64c', 0, 0, 0),
(11, 'Jan', 'Kowalski', 'jasiu345@wp.pl', 'f4b22d3e7560859241353c6b9b88a528', 0, 0, 0),
(12, 'Grzegorz', 'brzÄ™czyszczykiewicz', 'brzÄ™czyszczykiewicz@gmail.com', 'f3890a0544ed88a28c8e01cd6e1f5c4d', 0, 0, 0);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `users with gmail email`
-- (Zobacz poniżej rzeczywisty widok)
--
CREATE TABLE `users with gmail email` (
`Name` varchar(255)
,`Surname` varchar(255)
,`Email` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktura widoku `users with gmail email`
--
DROP TABLE IF EXISTS `users with gmail email`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users with gmail email`  AS  select `user`.`name` AS `Name`,`user`.`surname` AS `Surname`,`user`.`email` AS `Email` from `user` where (`user`.`email` like '%@gmail.com') ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `adres`
--
ALTER TABLE `adres`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `firma`
--
ALTER TABLE `firma`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adres` (`adres`);

--
-- Indeksy dla tabeli `image`
--
ALTER TABLE `image`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `konto`
--
ALTER TABLE `konto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transakcje` (`ostatnia_transakcja`);

--
-- Indeksy dla tabeli `proxy_user_image`
--
ALTER TABLE `proxy_user_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `image_id` (`image_id`);

--
-- Indeksy dla tabeli `transakcja`
--
ALTER TABLE `transakcja`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `konto` (`konto`),
  ADD KEY `adres` (`adres`),
  ADD KEY `firma` (`firma`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `adres`
--
ALTER TABLE `adres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `firma`
--
ALTER TABLE `firma`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `image`
--
ALTER TABLE `image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT dla tabeli `konto`
--
ALTER TABLE `konto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `proxy_user_image`
--
ALTER TABLE `proxy_user_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `transakcja`
--
ALTER TABLE `transakcja`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `adres`
--
ALTER TABLE `adres`
  ADD CONSTRAINT `adres_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`adres`);

--
-- Ograniczenia dla tabeli `firma`
--
ALTER TABLE `firma`
  ADD CONSTRAINT `firma_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`firma`);

--
-- Ograniczenia dla tabeli `konto`
--
ALTER TABLE `konto`
  ADD CONSTRAINT `konto_ibfk_1` FOREIGN KEY (`id`) REFERENCES `user` (`konto`);

--
-- Ograniczenia dla tabeli `proxy_user_image`
--
ALTER TABLE `proxy_user_image`
  ADD CONSTRAINT `proxy_user_image_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `proxy_user_image_ibfk_2` FOREIGN KEY (`image_id`) REFERENCES `image` (`id`);

--
-- Ograniczenia dla tabeli `transakcja`
--
ALTER TABLE `transakcja`
  ADD CONSTRAINT `transakcja_ibfk_1` FOREIGN KEY (`id`) REFERENCES `konto` (`ostatnia_transakcja`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
