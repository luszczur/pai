<!doctype html>
<?php

session_start();
	
	if(!(isset($_SESSION['zalogowany'])) && ($_SESSION['zalogowany'] == false))
	{
		header("Location: /pai/");
	}

?>


<head>
	<link rel="stylesheet" type="text/css" href="/pai/public/css/logged.css" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title> TWOJA PLAZMA REKLAMOWA! </title>

</head>
<body>
<div class="container1">
<div class="header">
<b>ZALOGOWANY</b>

</div></div>

<div class="container2">
<div class="logout">
<form action="/pai/?page=logout" method="POST">
<button id="uniqueButton2" class="button">Wyloguj!</button>
</form>
</div></div>

<div class="container3">
<div class="logged">
<?php echo "<b><p>Witaj ".$_SESSION['name']." ".$_SESSION['surname']."!</p></b>"; 
?>
<div class="upload">
<form action="/pai/?page=upload" method="POST" enctype="multipart/form-data">
<b>Dodaj swoja reklame do własnej plazmy reklamowej!</b>

<input type="file" name="image">

<button type="submit" id="uniqueButton4" class="button">Dodaj reklamę!</button>
</form></div>
<div class="plazma">
<form action="/pai/views/DisplayController/display.php" method="POST">
<b>Przejdź do swojej własnej plazmy reklamowej!</b>

<button id="uniqueButton3" class="button">Twoja Plazma Reklamowa!</button>
</form></div>
<div class="danepersonalne">
<form action="/pai/?page=personaldata" method="POST">
<b>Zobacz i zmień dane swojego konta!</b>

<button id="uniqueButton6" class="button">Dane personalne!</button>
</form>
</div>
</div>
</div>


<div class="container4">
<div class="footer">
<b>Strona wykonana przez: Łukasz Szczurek :)</b>

</div>		
</div>

</body>
</html>