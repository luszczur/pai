<?php

require_once 'controllers/DefaultController.php';

class Routing
{
    public $routes = [];

    public function __construct()
    {
        $this->routes = [
            'main' => [
                'controller' => 'DefaultController',
                'action' => 'main'
            ],
            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],
            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ],
            'register' => [
                'controller' => 'RegisterController',
                'action' => 'register'
            ],
            'logged' => [
                'controller' => 'LoggedController',
                'action' => 'logged'
            ],
            'display' => [
                'controller' => 'LoggedController',
                'action' => 'display'
            ],
        ];
    }

    public function run()
    {
        $page = isset($_GET['page'])
            && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'main';

        if ($this->routes[$page]) {
            $class = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $class;
            $object->$action();
        }
    }

}