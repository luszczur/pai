<?php

require_once "AppController.php";

require_once __DIR__.'/../model/User.php';
require_once __DIR__.'/../model/UserMapper.php';


class DefaultController extends AppController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function main()
    {
        $this->render('main');
    }

    public function login()
    {
        //echo "hello world";
		
		$mapper = new UserMapper();

        $user = null;

        if ($this->isPost()) {
			
			//echo "hello world2";
            $user = $mapper->getUser($_POST['email']);

            if(!$user) {
                echo "jakis tekst1";
				return $this->render('main', ['message' => ['Email not recognized']]);
            }

            if($user->getPassword() !== $_POST['password']) {
                return $this->render('main', ['message' => ['Wrong password']]);
            }else{
                //echo "hello world3";
				$_SESSION["id"] = $user->getEmail();
                $_SESSION["role"] = $user->getRole();

                //$url = "http://$_SERVER[HTTP_HOST]/";
                //header("Location: {$url}?page=logged");
				header("Location: views/RegisterController/register.php");
                exit();
            }
        }

        $this->render('main');
    }

    public function logout()
    {
        session_unset();
        session_destroy();

        $this->render('main', ['text' => 'You have been successfully logged out!']);
    }
}